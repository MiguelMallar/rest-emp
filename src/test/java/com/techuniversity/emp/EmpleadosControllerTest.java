package com.techuniversity.emp;

import com.techuniversity.emp.controllers.EmpleadosController;
import com.techuniversity.emp.utils.BadSeparator;
import com.techuniversity.emp.utils.EstadosPedido;
import com.techuniversity.emp.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class EmpleadosControllerTest {

    @Autowired
    EmpleadosController empleadosController;

 /*   @Test
    public void testCadena(){
        String esperado = "L.U.Z.D.E.L.S.O.L";
        assertEquals(esperado, empleadosController.getCadena("luz del sol", "."));

    }
*/
    @Test
    public void testCadenaBadSparator(){
        try{
            String esperado = "L.U.Z.D.E.L.S.O.L";
            empleadosController.getCadena("luz del sol", " ");
            fail("Se esperaba bad separator");
        } catch (BadSeparator badSeparator) {

        }
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, -3, 15, Integer.MAX_VALUE})
    public void testImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {""," "})
    public void testBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }


    @ParameterizedTest
    @NullSource
    @EmptySource
    public void testBlancoNull(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {""," ", "\t", "\n"})
    public void testBlancoNull1(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido estado){
        assertTrue(Utilidades.valorarEstadoPedido(estado));
    }
}
