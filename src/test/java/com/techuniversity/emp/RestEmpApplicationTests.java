package com.techuniversity.emp;

import com.techuniversity.emp.controllers.EmpleadosController;
import com.techuniversity.emp.utils.BadSeparator;
import com.techuniversity.emp.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class RestEmpApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	EmpleadosController empleadosController;
	@Test
	public void testHome(){
		String result = empleadosController.home();
		assertEquals("Don´t worry about it", result);
	}

	@Test
	public void testGetCadena() throws BadSeparator {
		String result = Utilidades.getCadena(" Miguel Mallar ", "."	);
		System.out.println(result);
		//assertEquals("M.I.G.U.E.L.M.A.L.L.A.R", result);
	}
}
