package com.techuniversity.emp.repositorios;

import com.techuniversity.emp.model.Empleado;
import com.techuniversity.emp.model.Empleados;
import com.techuniversity.emp.model.Formacion;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class EmpleadosDAO {
    private static Empleados lista = new Empleados();
    static {
        Formacion form1 = new Formacion("2021", "Partitioner");
        Formacion form2 = new Formacion("1998", "Curso Back");
        Formacion form3 = new Formacion("2018", "Paint");
        ArrayList<Formacion> una = new ArrayList<Formacion>();
        ArrayList<Formacion> dos = new ArrayList<Formacion>();
        ArrayList<Formacion> todas = new ArrayList<Formacion>();
        una.add(form1);
        dos.add(form1);
        dos.add(form2);
        todas.add(form3);
        todas.addAll(dos);
        lista.getListaEmpleados().add(new Empleado(1, "Pepe", "Lopez", "pepe@gmail.com", una));
        lista.getListaEmpleados().add(new Empleado(2, "Paco", "Perez", "paquito@gmail.com", dos));
        lista.getListaEmpleados().add(new Empleado(3, "Antonio", "Martinez", "toni@gmail.com", todas));
    }
    public Empleados getAllEmpleados(){
            return lista;
    }

    public Empleado getEmpleado(int id){
            for (Empleado emp: lista.getListaEmpleados()){
                if(emp.getId() == id){
                    return emp;
                }
            }
            return null;
    }

    public void addEmpleado(Empleado emp){
            lista.getListaEmpleados().add(emp);
    }

    public Empleado updEmpleado(Empleado emp){
        Empleado currentEmp = getEmpleado(emp.getId());
        if(currentEmp != null) {
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmail(emp.getEmail());
        }
        return currentEmp;
    }

    public Empleado updEmpleado(int id, Empleado emp){
        Empleado currentEmp = getEmpleado(id);
        if(currentEmp != null) {
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmail(emp.getEmail());
        }
        return currentEmp;
    }

    public void deleteEmpleado(int id){
        Iterator itr = lista.getListaEmpleados().iterator();
        while(itr.hasNext()){
            Empleado emp = (Empleado) itr.next();
            if(id == emp.getId()){
                itr.remove();
                break;
            }
        }
    }

    public Empleado softUpdEmpleado(int id, Map<String, Object> updates){
        Empleado current = getEmpleado(id);
        if(current != null){
            for (Map.Entry<String, Object> update : updates.entrySet()){
                switch (update.getKey()){
                    case "nombre":
                        current.setNombre(update.getValue().toString());
                        break;
                    case "apellidos":
                        current.setApellidos(update.getValue().toString());
                        break;
                    case "email":
                        current.setEmail(update.getValue().toString());
                        break;
                }
            }
        }
        return current;
    }
    public List<Formacion> getFormacionEmpleado(int id){
        /*
        for(Empleado emp : lista.getListaEmpleados()){
            if(emp.getId() == id){
                return emp.getFormaciones();
            }
        }
         */
        Empleado emp = getEmpleado(id);
        if(emp != null){
            ArrayList<Formacion> formaciones = emp.getFormaciones();
            return formaciones;
        }
        return null;
    }

    public Empleado addFormacionEmpleado(int id, Formacion formacion){
        Empleado current = getEmpleado(id);
        if(current != null)
            current.getFormaciones().add(formacion);
        return current;
    }


}
